#!env node

const fetch = require('node-fetch');

(async () => {
  if (process.argv.length !== 5) {
    console.log(`USAGE: ${process.argv[1]} cloudId conversationId accessToken`)
    process.exit(1);
  }
  const [, , cloudId, conversationId, token] = process.argv;
  const response = await fetch(`http://localhost:3000/archive-conversation?token=${token}`, {
      method: 'PUT',
      headers: {
        'content-type': 'application/json',
      },
      body: JSON.stringify({cloudId, conversationId})
    }
  );
  const text = await response.text();
  console.log(`Status code: ${response.status}`);
  console.log(`Response: ${text}`);
})().catch(e => {
  console.error(e);
  process.exit(1);
});
