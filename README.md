1. Install nvm. `nvm install`
2. Install yarn. `npm install yarn -g`
3. Build the app. `yarn && yarn build`
4. Download and run ngrok, pointing to port 3000
5. Run the app with `yarn start`
6. Create an app in [DAC app management](https://developer.atlassian.com/apps/).
7. Enable 3lo with callback url `https://<your-subdomain>.ngrok.io/callback` in DAC. 
8. Navigate to `https://<your-subdomain>.ngrok.io/` and copy the CLIENT-ID and SECRET from DAC to kickoff the flow.
9. Use the scripts in the `scripts/` directory to attempt operations not possible through the UI (mostly attempting actions you don't have permission to do).

For development run `yarn watch`.
