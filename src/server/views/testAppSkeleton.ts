import * as serialize from 'serialize-javascript';

export type SiteInfo = {
  id: string;
  name: string;
};

export type ConversationInfo = {
  id: string;
  name: string;
};

type CallbackScreenProps = {
  sites: Array<SiteInfo>;
  conversations: {
    [siteId: string]: Array<ConversationInfo>
  }
};

export default (page: 'landing' | 'callback' | 'jira' | 'confluence', environmentQualifier = '', props?: CallbackScreenProps) => {
  return `
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>3lo Test App</title>
      <style>
        * {
          box-sizing: border-box;
        }

        .appkey {
          margin-bottom: 12px;
          width: 640px;
        }

        .appkey__button {
          margin-right: 12px !important;
          margin-top: 12px !important;
        }

        .appkey__start {
          margin-top: 24px !important;
        }

        .wrapper {
          padding: 12px;
        }

        .playground {
          margin-top: 24px;
        }

        .playground__field {
          padding-top: 12px;
        }

        .tabs {
          margin: 12px 0;
        }

        .tabs__tab-content {
          padding: 12px 0;
        }

        .conversation-meta {
          margin-top: 12px;
          max-width: 100vw;
        }

        .conversation-manage-button {
          margin-left: 12px !important;
        }
      </style>
    </head>
    <body>
      <div id="app-root"></div>
      <script>
        window.__ACCESS_TOKEN__ = ${serialize(props, { isJSON: true })}
        window.environmentQualifier = '${environmentQualifier}';
      </script>
      <script src="/${page}.bundle.js"></script>
    </body>
    </html>
  `;
};
