export const checkQueryToken = (req, res, next) => {
  if (req.query.token) {
    next();
  } else {
    next(new Error(`token query parameter is not set`));
  }
};
