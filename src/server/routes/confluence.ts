import * as assert from 'assert';
import * as express from 'express';
import fetch from 'node-fetch';
import { environmentQualifier } from '../util/environment';
import { checkQueryToken } from '../util/middleware';
import getTestAppSkeleton from '../views/testAppSkeleton';

const confluence = async (url: string, token: string, action: string, options = {}) => {
  if (!token) {
    throw new Error('access_token query parameter is missing');
  }
  const defaultOptions = {
    headers: {
      'content-type': 'application/json',
      'authorization': `Bearer ${token}`
    }
  };

  try {
    const response = await fetch(url, {
      ...defaultOptions,
      ...options
    });

    const body = await response.json();
    assert(response.ok, `Error while ${action} - status: ${response.status}, body: ${JSON.stringify(body)}`);
    return body;
  } catch (e) {
    console.error(e);
    throw new Error(`Error while ${action}: ${JSON.stringify(e)}`);
  }
};

export const setup = (app: express.Express) => {
  app.get('/playground/confluence', (req, res, next) => {
    if (!req.query.access_token) {
      return next(new Error('access_token query parameter is missing'));
    }

    res.setHeader('Content-Type', 'text/html');
    res.send(getTestAppSkeleton('confluence', environmentQualifier));
  });

  app.get('/confluence/:site/space', checkQueryToken, async (req, res, next) => {
    const { token } = req.query;
    const { site } = req.params;

    try {
      const spaces = await confluence(`https://api${environmentQualifier}.atlassian.com/ex/confluence/${site}/rest/api/space`, token, 'fetching spaces');
      res.setHeader('Content-Type', 'application/json');
      res.send(spaces);
    } catch (e) {
      next(e);
    }
  });

  app.get('/confluence/:site/space/:space/content', async (req, res, next) => {
    const { token } = req.query;
    const { site, space } = req.params;

    try {
      const pages = await confluence(`https://api${environmentQualifier}.atlassian.com/ex/confluence/${site}/rest/api/space/${space}/content`, token, 'fetching pages');
      res.setHeader('Content-Type', 'application/json');
      res.send(pages);
    } catch (e) {
      next(e);
    }
  });

  app.post('/confluence/:site/space/:space/content/:page/comment', async (req, res, next) => {
    const { token } = req.query;
    const { site, space, page } = req.params;
    const { comment } = req.body;

    try {
      const addComment = await confluence(`https://api${environmentQualifier}.atlassian.com/ex/confluence/${site}/rest/api/content`, token, 'fetching pages', {
        method: 'POST',
        body: JSON.stringify({
          type: 'comment',
          container: {
            type: 'page',
            id: page,
          },
          body: {
            storage: {
              value: comment,
              representation: 'storage'
            }
          }
        })
      });
      res.setHeader('Content-Type', 'application/json');
      res.send(addComment);
    } catch (e) {
      next(e);
    }
  });
};
