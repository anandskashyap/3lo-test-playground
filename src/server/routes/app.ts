import * as assert from 'assert';
import * as express from 'express';
import * as fs from 'fs';
import fetch from 'node-fetch';
import { stringify } from 'querystring';

import { environmentQualifier } from '../util/environment';

const getRequestOrigin = (req: express.Request): string => {
  return `${req.protocol}://${req.get('host')}`;
};

const getTokens = async (
  authCode: string,
  baseUrl: string,
  clientId: string,
  clientSecret: string
): Promise<{access: string, refresh: string}> => {
  const response = await fetch(`https://auth${environmentQualifier}.atlassian.com/oauth/token`, {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      grant_type: 'authorization_code',
      client_id: clientId,
      client_secret: clientSecret,
      code: authCode,
      redirect_uri: `${baseUrl}/callback`
    })
  });

  if (!response.ok) {
    const responseText = await response.text();
    throw new Error(`Bad response from auth0 when fetching access token (${response.status}): ${responseText}`);
  }

  const {access_token: access, refresh_token: refresh} = await response.json();
  return {access, refresh};
};

export const setup = (app: express.Express) => {
  app.get('/descriptor', (req, res) => {
    const buffer = fs.readFileSync('./descriptor.json');
    const descriptorFileJSON = JSON.parse(buffer.toString());
    descriptorFileJSON.baseUrl = getRequestOrigin(req);
    res.setHeader('Content-Type', 'application/json');
    res.json(descriptorFileJSON);
  });

  app.get('/callback', async (req, res, next) => {
    if (!req.query.code) {
      res.status(400);
      res.send('No code param');
      return;
    } else if (!req.query.state) {
      res.status(401);
      res.send('incorrect state param');
      return;
    }

    let state;

    try {
      state = JSON.parse(req.query.state);
      assert.strictEqual(state.custom, 'some data');
      assert(state.id, 'Client ID is unknown');
      assert(state.secret, 'Client secret is unknown');
    } catch (ex) {
      next(ex);
      return;
    }

    try {
      const {access, refresh} = await getTokens(
        req.query.code,
        getRequestOrigin(req),
        state.id,
        state.secret
      );

      const query = {
        access_token: access,
        refresh_token: refresh,
        client_id: state.id,
        client_secret: state.secret
      };

      res.redirect(`/playground/${state.playground}?${stringify(query)}`);
    } catch (err) {
      next(err);
    }
  });
};
