import * as assert from 'assert';
import * as express from 'express';
import fetch from 'node-fetch';

import { environmentDomain, environmentQualifier } from '../util/environment';
import { checkQueryToken } from '../util/middleware';
import getTestAppSkeleton from '../views/testAppSkeleton';

const getJiraURL = async (site: string, token: string, action: string, url: string, initOpts = {}): Promise<any> => {
  const fetchInitOpts = {
    headers: {
      'content-type': 'application/json',
      'authorization': `Bearer ${token}`
    }
  };

  const response = await fetch(`https://api${environmentQualifier}.atlassian.com/ex/jira/${site}/rest/api/2/${url}`, {
    ...initOpts,
    ...fetchInitOpts
  });

  assert(response.ok, `Bad response from JIRA API when ${action}: ${response.status}`);
  return await response.json();
};

export const setup = (app: express.Express) => {
  app.get('/jira/projects/:site', checkQueryToken, async (req, res, next) => {
    try {
      const json = await getJiraURL(req.params.site, req.query.token, 'fetching the projects list', 'project');
      res.json(json);
    } catch (err) {
      next(err);
    }
  });

  app.get('/jira/issues/:site/:project', checkQueryToken, async (req, res, next) => {
    const {site, project} = req.params;

    try {
      const json = await getJiraURL(site, req.query.token, 'fetching the issues list', `search?jql=project=${project}`);
      res.json(json);
    } catch (err) {
      next(err);
    }
  });

  app.post('/jira/comment/:site/:issue', checkQueryToken, async (req, res, next) => {
    const {site, issue} = req.params;
    const {comment, siteName} = req.body;

    try {
      const json = await getJiraURL(site, req.query.token, 'adding the comment', `issue/${issue}/comment`, {
        method: 'POST',
        body: JSON.stringify({
          body: `Comment from Jira request - ${comment}`
        })
      });

      res.json({...json, ...{ commentLink: `https://${siteName}.${environmentDomain}/browse/${issue}`}});
    } catch (err) {
      next(err);
    }
  });

  app.get('/playground/jira', async (req, res, next) => {
    if (!req.query.access_token) {
      next(new Error('access_token query parameter is missing'));
      return;
    }

    res.setHeader('Content-Type', 'text/html');
    res.send(getTestAppSkeleton('jira', environmentQualifier));
  });
};
