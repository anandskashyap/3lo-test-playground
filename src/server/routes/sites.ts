import * as assert from 'assert';
import * as express from 'express';
import fetch from 'node-fetch';
import { environmentQualifier } from '../util/environment';
import { checkQueryToken } from '../util/middleware';

export const setup = (app: express.Express) => {
  app.get('/introspection/sites', checkQueryToken, async (req, res, next) => {
    try {
      const response = await fetch(`https://api${environmentQualifier}.atlassian.com/oauth/token/accessible-resources`, {
        headers: {
          'content-type': 'application/json',
          'authorization': `Bearer ${req.query.token}`
        }
      });

      assert(response.ok, `Bad response from Introspection API: ${response.status}`);

      const json = await response.json();
      res.json(json);
    } catch (err) {
      next(err);
    }
  });
};
