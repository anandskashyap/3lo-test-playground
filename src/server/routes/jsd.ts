import * as assert from 'assert';
import * as express from 'express';
import fetch from 'node-fetch';

import { environmentDomain, environmentQualifier } from '../util/environment';
import { checkQueryToken } from '../util/middleware';

const getJsdUrl = async (site: string, token: string, action: string, url: string, initOpts = {}): Promise<any> => {
  const fetchInitOpts = {
    headers: {
      'content-type': 'application/json',
      'accept': 'application/json',
      'authorization': `Bearer ${token}`
    }
  };
  const jsdUrl = `https://api${environmentQualifier}.atlassian.com/ex/jira/${site}/rest/servicedeskapi/request/${url}`;
  const response = await fetch(jsdUrl, {
    ...initOpts,
    ...fetchInitOpts
  });

  assert(response.ok, `Bad response from JSD API when ${action}: ${response.status}`);
  return await response.json();
};

export const setup = (app: express.Express) => {
  app.post('/jsd/comment/:site/:issue', checkQueryToken, async (req, res, next) => {
    const {site, issue} = req.params;
    const {comment, siteName} = req.body;

    try {
      const json = await getJsdUrl(site, req.query.token, 'adding the comment to JSD request', `${issue}/comment`, {
        method: 'POST',
        body: JSON.stringify({
          body: `Comment from JSD request- ${comment}`,
          public: true
        })
      });
      res.json({...json, ...{ commentLink: `https://${siteName}.${environmentDomain}/servicedesk/customer/portal/1/${issue}`}});
    } catch (err) {
      next(err);
    }

  });

}