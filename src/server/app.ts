import * as bodyParser from 'body-parser';
import * as express from 'express';

import { setup as setupApp } from './routes/app';
import { setup as setupJira } from './routes/jira';
import { setup as setupSites } from './routes/sites';
import { setup as setupConfluence } from './routes/confluence';
import { setup as setupJsd } from './routes/jsd';
import { environmentQualifier } from './util/environment';
import getTestAppSkeleton from './views/testAppSkeleton';

const app = express();
app.set('trust proxy', 'loopback');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.setHeader('Content-Type', 'text/html');
  res.send(getTestAppSkeleton('landing', environmentQualifier));
});

setupJira(app);
setupConfluence(app);
setupJsd(app);
setupApp(app);
setupSites(app);

app.all('*', (err, req, res, next) => {
  res.send(`Error occured: ${err.message}`);
});

app.listen(3000, () => console.log('Listening on port 3000'));

process.on('unhandledRejection', (reason: Error) => {
  console.error('Unhandled promise rejection: %s', reason.stack);
  process.exit(1);
});

process.on('uncaughtException', (err: Error) => {
  console.error('Uncaught exception: %s', err.message);
});
