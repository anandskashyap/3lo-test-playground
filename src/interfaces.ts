export type AppCredentialsProps = {
  id: string;
  secret: string;
};
