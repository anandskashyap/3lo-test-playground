import '@atlaskit/css-reset';
import 'babel-polyfill';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import ConfluenceComment from './components/confluence-comment';
import ConfluencePage from './components/confluence-page';
import ConfluenceSpace from './components/confluence-space';
import Credentials from './components/credentials';
import { ConfluenceSite } from './components/site';
import Wrapper from './components/wrapper';

import { DataProvider } from './util';

export interface Props {
  credentials: {
    id: string;
    secret: string;
  };
  tokens: {
    access: string;
    refresh: string;
  };
}

export interface State {
  accessToken: string;
  spaceKey: string | null;
  pageId: string | null;
  siteId: string | null;
}

class ConfluencePlayground extends React.Component<Props, State> {
  private dataProvider: DataProvider;

  constructor(props: Props) {
    super(props);

    this.dataProvider = new DataProvider(this.props.tokens.access);
    this.state = {
      siteId: null,
      spaceKey: null,
      pageId: null,
      accessToken: props.tokens.access
    };
  }

  onAccessTokenChange = (accessToken: string) => {
    this.dataProvider.setAccessToken(accessToken);
  }

  onSiteSelect = (siteId: string) => {
    this.setState({
      siteId,
      spaceKey: null,
      pageId: null
    });
  }

  onSpaceSelect = (spaceKey: string) => {
    this.setState({
      spaceKey,
      pageId: null
    });
  }

  onPageSelect = (pageId: string) => {
    this.setState({ pageId });
  }

  render(): JSX.Element {
    const {credentials, tokens} = this.props;
    const {
      accessToken,
      spaceKey,
      siteId,
      pageId
    } = this.state;

    return (
      <Wrapper>
        <Credentials
          clientId={credentials.id}
          clientSecret={credentials.secret}
          accessToken={tokens.access}
          refreshToken={tokens.refresh}
          onAccessTokenChange={this.onAccessTokenChange}
        />
        <div className="playground">
          <h1>Playground</h1>
          <ConfluenceSite
            cloudId={siteId}
            accessToken={accessToken}
            onSiteSelect={this.onSiteSelect}
          />
          <ConfluenceSpace
            cloudId={siteId}
            accessToken={accessToken}
            onSpaceSelect={this.onSpaceSelect}
          />
          <ConfluencePage
            cloudId={siteId}
            spaceKey={spaceKey}
            accessToken={accessToken}
            onPageSelect={this.onPageSelect}
          />
          <ConfluenceComment
            accessToken={accessToken}
            site={siteId}
            space={spaceKey}
            page={pageId}
          />
        </div>
      </Wrapper>
    );
  }
}

declare global {
  // noinspection JSUnusedGlobalSymbols
  interface Window {
    environmentQualifier: string;
  }
}

const query = new URLSearchParams(location.search);

ReactDOM.render(
  <ConfluencePlayground
    credentials={{
      id: query.get('client_id')!,
      secret: query.get('client_secret')!
    }}
    tokens={{
      access: query.get('access_token')!,
      refresh: query.get('refresh_token')!
    }}
  />,
  document.getElementById('app-root')
);
