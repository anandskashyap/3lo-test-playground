import * as assert from 'assert';
import * as React from 'react';
import * as ReactDOM from 'react-dom';

import {FieldTextStateless} from '@atlaskit/field-text';
import SingleSelect from '@atlaskit/single-select';
import Button from '@atlaskit/button';
import Tabs from '@atlaskit/tabs';
import '@atlaskit/css-reset';
import 'babel-polyfill';

import {DataProvider} from './util';
import Wrapper from './components/wrapper';
import Credentials from './components/credentials';

export type SiteInfo = {
  id: string;
  name: string;
};

export type ConversationInfo = {
  id: string;
  name: string;
};

type CallbackScreenProps = {
  credentials: {
    id: string;
    secret: string;
  };
  tokens: {
    access: string;
    refresh: string;
  };
  sites: Array<SiteInfo>;
  conversations: {
    [siteId: string]: Array<ConversationInfo>
  }
};

type CallbackScreenStateSelected = {
  siteId: string;
  conversationId?: string;
  conversation?: any;
};

type CallbackScreenState = {
  selected?: CallbackScreenStateSelected;
};

const getSiteIndexById = (sites: Array<SiteInfo>, siteId: string): number => {
  for (let i = 0; i < sites.length; i++) {
    if (sites[i].id === siteId) {
      return i;
    }
  }

  throw new Error(`Unknown site id: ${siteId}`);
};

class CallbackScreen extends React.Component<CallbackScreenProps, CallbackScreenState> {
  private queueUpdateId: any;
  private dataProvider: DataProvider;
  state: CallbackScreenState = {};

  constructor(props) {
    super(props);
    this.dataProvider = new DataProvider(this.props.tokens.access);
  }

  onAccessTokenChange = (accessToken: string) => {
    this.dataProvider.setAccessToken(accessToken);
  }

  onUpdateQueue = async () => {
    const {messages} = await this.dataProvider.fetch('/webhook/queue');

    for (const {webhookId, message} of messages) {
      new Notification(webhookId, {
        body: message.message.text
      });
    }
  }

  onConversationSelectionChange = (siteId) => async (action) => {
    await this.updateSelectedConversation(siteId, action.item.value);
  }

  onSiteSelectectionChange = (tab, index) => {
    const {conversations, sites} = this.props;
    const {selected} = this.state;

    // same site
    if (selected && selected.siteId === sites[index].id) {
      return;
    }

    this.setState({
      selected: {
        siteId: sites[index].id
      }
    });
  }

  setUpWebhook = async () => {
    const {conversationId, siteId} = this.state.selected!;

    const webhookId = prompt('Insert webhook id');
    if (!webhookId) {
      return;
    }

    const pattern = prompt('Specify message pattern', '.*');
    if (!pattern) {
      return;
    }

    await this.dataProvider.fetch('/webhook', {
      method: 'PUT',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify({
        webhookId,
        conversationId,
        cloudId: siteId,
        pattern
      })
    });

    await this.updateSelectedConversation(siteId, conversationId);
  }

  deleteWebhook = async () => {
    const webhookId = prompt('Insert webhook id');
    const {conversationId, siteId} = this.state.selected!;

    if (!webhookId) {
      return;
    }

    await this.dataProvider.fetch('/webhook', {
      method: 'DELETE',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify({
        webhookId,
        conversationId,
        cloudId: siteId
      })
    });

    await this.updateSelectedConversation(siteId, conversationId);
  }

  archiveConversation = async () => {
    if (!confirm('This will mark conversation as archived. Are you sure?')) {
      return;
    }

    const {conversationId, siteId} = this.state.selected!;

    await this.dataProvider.fetch('/archive-conversation', {
      method: 'PUT',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify({
        conversationId,
        cloudId: siteId
      })
    });

    await this.updateSelectedConversation(siteId, conversationId);
  }

  unarchiveConversation = async () => {
    const {conversationId, siteId} = this.state.selected!;

    await this.dataProvider.fetch('/unarchive-conversation', {
      method: 'PUT',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify({
        conversationId,
        cloudId: siteId
      })
    });

    await this.updateSelectedConversation(siteId, conversationId);
  }

  async updateSelectedConversation(siteId, conversationId): Promise<any> {
    this.setState({
      selected: {
        siteId,
        conversationId
      }
    });

    const conversation = await this.dataProvider.fetch('/conversation', {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify({
        conversationId,
        cloudId: siteId
      })
    });

    this.setState({
      selected: {
        siteId,
        conversationId,
        conversation
      }
    });
  }

  renderTabs(): JSX.Element {
    const {conversations, sites} = this.props;
    const {selected} = this.state;
    const selectedTabIndex = selected ? getSiteIndexById(sites, selected.siteId) : null;

    const tabs = sites.map((site, index) => {
      const conversationItems = [{
        items: conversations[site.id].map((conversation) => ({
          content: conversation.name,
          value: conversation.id
        }))
      }];

      return {
        label: site.name,
        content: (
          <div className="tabs__tab-content">
            <SingleSelect
              key={`room-select-${site.id}`}
              items={conversationItems}
              placeholder="Choose a conversation"
              onSelected={this.onConversationSelectionChange(site.id)}
            />

            {this.renderConversationButtons()}
            {this.renderConversationMeta()}
          </div>
        )
      };
    });

    return (
      <div className="tabs">
        <Tabs
          tabs={tabs}
          selected={selectedTabIndex}
          onSelect={this.onSiteSelectectionChange}
        />
      </div>
    );
  }

  renderConversationButtons(): JSX.Element | null {
    const {selected} = this.state;

    if (!selected || !selected.conversation) {
      return null;
    }

    const {conversation, webhooks} = selected.conversation;
    const archiveButtonTitle = conversation.isArchived ? 'Unarchive conversation' : 'Archive conversation';
    const archiveButtonHandler = conversation.isArchived ? this.unarchiveConversation : this.archiveConversation;

    return (
      <span className="conversation-manage-buttons">
        <Button
          className="conversation-manage-button"
          onClick={archiveButtonHandler}>{archiveButtonTitle}</Button>
        <Button
          className="conversation-manage-button"
          onClick={this.setUpWebhook}>Create/update new webhook</Button>
        <Button
          className="conversation-manage-button"
          onClick={this.deleteWebhook}>Delete existing webhook</Button>
      </span>
    );
  }

  renderConversationMeta(): JSX.Element | null {
    const {selected} = this.state;

    if (!selected || !selected.conversationId) {
      return null;
    }

    if (!selected.conversation) {
      return (
        <div className="conversation-meta">Please wait...</div>
      );
    }

    return (
      <div className="conversation-meta">
        <pre>{JSON.stringify(selected.conversation, null, 2)}</pre>
      </div>
    );
  }

  async componentDidMount(): Promise<void> {
    await Notification.requestPermission();
    this.queueUpdateId = setInterval(this.onUpdateQueue, 5000);
  }

  componentWillUnmount(): void {
    if (this.queueUpdateId) {
      clearInterval(this.queueUpdateId);
      this.queueUpdateId = null;
    }
  }

  render(): JSX.Element {
    const {credentials, tokens} = this.props;

    return (
      <Wrapper>
        <Credentials
          clientId={credentials.id}
          clientSecret={credentials.secret}
          accessToken={tokens.access}
          refreshToken={tokens.refresh}
          onAccessTokenChange={this.onAccessTokenChange}
        />
        <div className="playground">
          <h1>Sites and conversations</h1>
          {this.renderTabs()}
        </div>
      </Wrapper>
    );
  }
}

declare global {
  // noinspection JSUnusedGlobalSymbols
  interface Window {
    __ACCESS_TOKEN__: CallbackScreenProps;
    environmentQualifier: string;
  }
}

const callbackScreenProps = window.__ACCESS_TOKEN__;
const query = new URLSearchParams(location.search);

ReactDOM.render(
  <CallbackScreen
    credentials={{
      id: query.get('client_id'),
      secret: query.get('client_secret')
    }}
    tokens={{
      access: query.get('access_token'),
      refresh: query.get('refresh_token')
    }}
    {...callbackScreenProps}
  />,
  document.getElementById('app-root')
);
