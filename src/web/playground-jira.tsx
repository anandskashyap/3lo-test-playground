import '@atlaskit/css-reset';
import 'babel-polyfill';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Credentials from './components/credentials';
import JiraIssue from './components/jira-issue';
import JiraProject from './components/jira-project';
import { JiraSite } from './components/site';
import Wrapper from './components/wrapper';

import { DataProvider } from './util';
import ProductComment from './components/product-comment';

export interface Props {
  credentials: {
    id: string;
    secret: string;
  };
  tokens: {
    access: string;
    refresh: string;
  };
}

export interface State {
  accessToken: string;
  projectKey: string | null;
  issueKey: string | null;
  siteId: string | null;
  siteName: string | null;
}

class JiraPlayground extends React.Component<Props, State> {
  private dataProvider: DataProvider;

  constructor(props: Props) {
    super(props);

    this.dataProvider = new DataProvider(this.props.tokens.access);
    this.state = {
      siteId: null,
      siteName: null,
      projectKey: null,
      issueKey: null,
      accessToken: props.tokens.access
    };
  }

  onAccessTokenChange = (accessToken: string) => {
    this.dataProvider.setAccessToken(accessToken);
  }

  onSiteSelect = (siteId: string, siteName: string) => {
    this.setState({
      siteId,
      siteName,
      projectKey: null,
      issueKey: null
    });
  }

  onProjectSelect = (projectKey: string) => {
    this.setState({
      projectKey,
      issueKey: null
    });
  }

  onIssueSelect = (issueKey: string) => {
    this.setState({issueKey});
  }

  render(): JSX.Element {
    const {credentials, tokens} = this.props;
    const {
      accessToken,
      projectKey,
      siteId,
      siteName,
      issueKey
    } = this.state;

    return (
      <Wrapper>
        <Credentials
          clientId={credentials.id}
          clientSecret={credentials.secret}
          accessToken={tokens.access}
          refreshToken={tokens.refresh}
          onAccessTokenChange={this.onAccessTokenChange}
        />
        <div className="playground">
          <h1>Playground</h1>
          <JiraSite
            cloudId={siteId}
            accessToken={accessToken}
            onSiteSelect={this.onSiteSelect}
          />
          <JiraProject
            accessToken={accessToken}
            site={siteId}
            onProjectSelect={this.onProjectSelect}
          />
          <JiraIssue
            accessToken={accessToken}
            site={siteId}
            project={projectKey}
            onIssueSelect={this.onIssueSelect}
          />
          <ProductComment
            accessToken={accessToken}
            site={siteId}
            project={projectKey}
            siteName={siteName}
            issue={issueKey}
            productProps={{
              productName: 'Jira',
              commentCreateUrl: 'jira/comment'
            }}
          />
          <ProductComment
            accessToken={accessToken}
            site={siteId}
            siteName={siteName}
            project={projectKey}
            issue={issueKey}
            productProps={{
              productName: 'JSD',
              commentCreateUrl: 'jsd/comment'
            }}
          />
        </div>
      </Wrapper>
    );
  }
}

declare global {
  // noinspection JSUnusedGlobalSymbols
  interface Window {
    environmentQualifier: string;
  }
}

const query = new URLSearchParams(location.search);

ReactDOM.render(
  <JiraPlayground
    credentials={{
      id: query.get('client_id')!,
      secret: query.get('client_secret')!
    }}
    tokens={{
      access: query.get('access_token')!,
      refresh: query.get('refresh_token')!
    }}
  />,
  document.getElementById('app-root')
);
