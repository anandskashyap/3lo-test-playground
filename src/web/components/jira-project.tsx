import * as React from 'react';

import SingleSelect from '@atlaskit/single-select';
import {DataProvider} from '../util';

export interface Project {
  key: string;
  name: string;
}

export interface Props {
  accessToken: string;
  onProjectSelect: (projectKey: string) => any;
  site: string | null;
}

export interface State {
  fetching?: boolean;
  projects: Array<Project>;
}

class JiraProject extends React.Component<Props, State> {
  private dataProvider: DataProvider;

  constructor(props: Props) {
    super(props);

    this.dataProvider = new DataProvider(props.accessToken);
    this.state = {
      projects: []
    };
  }

  onProjectSelect = (data) => {
    const projectKey = data.item.value;
    this.props.onProjectSelect(projectKey);
  }

  async getSiteProjects(): Promise<void> {
    this.setState({fetching: true});

    try {
      const json = await this.dataProvider.fetch(`/jira/projects/${this.props.site}`);
      const projects: Array<Project> = json.map((project) => {
        return {
          key: project.key,
          name: project.name
        };
      });

      this.setState({projects});
    } finally {
      this.setState({fetching: false});
    }
  }

  componentWillReceiveProps(nextProps: Props): void {
    if (this.props.accessToken !== nextProps.accessToken) {
      this.dataProvider.setAccessToken(nextProps.accessToken);
    }
  }

  async componentDidMount(): Promise<void> {
    if (this.props.site) {
      await this.getSiteProjects();
    }
  }

  async componentDidUpdate(prevProps: Props): Promise<void> {
    if (this.props.site !== prevProps.site) {
      await this.getSiteProjects();
    }
  }

  render(): JSX.Element | null {
    const {site} = this.props;
    const {fetching, projects} = this.state;

    if (!site) {
      return null;
    }

    if (fetching) {
      return (
        <div className="playground__field">
          <span>Please wait...</span>
        </div>
      );
    }

    const items = [{
      items: projects.map((project) => ({
        content: project.name,
        value: project.key
      }))
    }];

    return (
      <div className="playground__field">
        <SingleSelect
          hasAutocomplete
          items={items}
          label="Project"
          onSelected={this.onProjectSelect}
        />
      </div>
    );
  }
}

export default JiraProject;
