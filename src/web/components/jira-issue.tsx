import * as React from 'react';

import SingleSelect from '@atlaskit/single-select';
import {DataProvider} from '../util';

export interface Issue {
  key: string;
  name: string;
}

export interface Props {
  accessToken: string;
  onIssueSelect: (issueKey: string) => any;
  project: string | null;
  site: string | null;
}

export interface State {
  fetching?: boolean;
  issues: Array<Issue>;
}

class JiraIssue extends React.Component<Props, State> {
  private dataProvider: DataProvider;

  constructor(props: Props) {
    super(props);

    this.dataProvider = new DataProvider(props.accessToken);
    this.state = {
      issues: []
    };
  }

  onIssueSelect = (data) => {
    const issueKey = data.item.value;
    this.props.onIssueSelect(issueKey);
  }

  async getIssues(): Promise<void> {
    const {project, site} = this.props;
    if (!project || !site) {
      return;
    }

    this.setState({fetching: true});

    try {
      const res = await this.dataProvider.fetch(`/jira/issues/${site}/${project}`);
      const issues: Array<Issue> = res.issues.map((issue) => {
        return {
          key: issue.key,
          name: `${issue.key}: ${issue.fields.summary}`
        };
      });

      this.setState({issues});
    } finally {
      this.setState({fetching: false});
    }
  }

  componentWillReceiveProps(nextProps: Props): void {
    if (this.props.accessToken !== nextProps.accessToken) {
      this.dataProvider.setAccessToken(nextProps.accessToken);
    }
  }

  async componentDidMount(): Promise<void> {
    await this.getIssues();
  }

  async componentDidUpdate(prevProps: Props): Promise<void> {
    if (this.props.site !== prevProps.site || this.props.project !== prevProps.project) {
      await this.getIssues();
    }
  }

  render(): JSX.Element | null {
    const {project, site} = this.props;
    const {fetching, issues} = this.state;

    if (!site || !project) {
      return null;
    }

    if (fetching) {
      return (
        <div className="playground__field">
          <span>Please wait...</span>
        </div>
      );
    }

    const items = [{
      items: issues.map((issue) => ({
        content: issue.name,
        value: issue.key
      }))
    }];

    return (
      <div className="playground__field">
        <SingleSelect
          hasAutocomplete
          droplistShouldFitContainer
          items={items}
          label="Issue"
          onSelected={this.onIssueSelect}
        />
      </div>
    );
  }
}

export default JiraIssue;
