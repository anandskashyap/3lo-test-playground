import SingleSelect from '@atlaskit/single-select';
import * as React from 'react';
import { Component } from 'react';
import { DataProvider } from '../util';

interface Props {
  cloudId: string | null;
  accessToken: string;
  onSpaceSelect: (spaceKey: string) => any;
}

interface Space {
  id: string;
  key: string;
  name: string;
  type: string;
  status: string;
}

interface State {
  fetching?: boolean;
  spaces: Array<Space>;
}

class ConfluenceSpace extends Component<Props, State> {
  private dataProvider: DataProvider;

  constructor(props: Props) {
    super(props);

    this.dataProvider = new DataProvider(props.accessToken);

    this.state = {
      fetching: false,
      spaces: []
    };
  }

  onSpaceSelect = (data) => {
    const spaceKey = data.item.value;
    this.props.onSpaceSelect(spaceKey);
  }

  async getSpaces(): Promise<void> {
    if (!this.props.cloudId) {
      return;
    }

    this.setState({fetching: true});

    try {
      const json = await this.dataProvider.fetch(`/confluence/${this.props.cloudId}/space`);
      const spaces: Array<Space> = json.results
        .map((space) => {
          return {
            id: space.id,
            key: space.key,
            name: space.name,
            type: space.type,
            status: space.status
          };
        });

      this.setState({ spaces });
    } finally {
      this.setState({ fetching: false });
    }
  }

  componentWillReceiveProps(nextProps: Props): void {
    if (this.props.accessToken !== nextProps.accessToken) {
      this.dataProvider.setAccessToken(nextProps.accessToken);
    }
  }

  async componentDidMount(): Promise<void> {
    await this.getSpaces();
  }

  async componentDidUpdate(prevProps: Props): Promise<void> {
    if (this.props.cloudId !== prevProps.cloudId) {
      await this.getSpaces();
    }
  }

  render(): JSX.Element | null {
    const { fetching, spaces } = this.state;

    if (fetching) {
      return (
        <div className="playground__field">
          <span>Please wait...</span>
        </div>
      );
    }

    const items = [{
      items: spaces.map((space) => ({
        content: space.name,
        value: space.key
      }))
    }];

    return (
      <div className="playground__field">
        <SingleSelect
          hasAutocomplete
          items={items}
          label="Space"
          onSelected={this.onSpaceSelect}
        />
      </div>
    );
  }
}

export default ConfluenceSpace;
