import * as React from 'react';

import Button from '@atlaskit/button';
import { FieldTextAreaStateless } from '@atlaskit/field-text-area';

import { DataProvider } from '../util';

export interface ProductProps {
  productName: string;
  commentCreateUrl: string;
}

export interface Props {
  accessToken: string;
  site: string | null;
  siteName: string | null;
  project: string | null;
  issue: string | null;
  productProps: ProductProps;
}

export interface State {
  saving?: boolean;
  comment?: string;
  commentId?: string;
  commentLink?: string;
}

class ProductComment extends React.Component<Props, State> {
  private dataProvider: DataProvider;
  state: State = {};

  constructor(props: Props) {
    super(props);
    this.dataProvider = new DataProvider(props.accessToken);
  }

  onComment = (evt) => {
    this.setState({comment: evt.target.value});
  }

  onCommentSend = async () => {
    const {site, issue, siteName, productProps: {commentCreateUrl}} = this.props;
    this.setState({saving: true});

    try {
      const {id, commentLink} = await this.dataProvider.fetch(`/${commentCreateUrl}/${site}/${issue}`, {
        method: 'POST',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify({
          comment: this.state.comment,
          siteName
        })
      });

      this.setState({commentId: id, commentLink});
    } finally {
      this.setState({saving: false});
    }
  }

  render(): JSX.Element | null {
    const {project, site, issue, productProps: {productName}} = this.props;
    const {saving, comment, commentId, commentLink} = this.state;

    if (!site || !project || !issue) {
      return null;
    }

    return (
      <div className="playground__field">
        <FieldTextAreaStateless
          label="Comment"
          onChange={this.onComment}
          enableResize
          shouldFitContainer
          minimumRows={5}
          value={comment || ''}
        />
        <Button
          isDisabled={saving}
          className="appkey__button"
          onClick={this.onCommentSend}>Add {`${productName}`} Comment</Button>
        {commentId && (
          <div className="playground__field">
            New {`${productName}`} comment created with id <b>#{commentId}</b>: <a target="_blank" href={`${commentLink}`}>link</a>
          </div>
        )}
      </div>
    );
  }
}

export default ProductComment;
