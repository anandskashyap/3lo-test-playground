import * as React from 'react';

import {FieldTextStateless} from '@atlaskit/field-text';
import Button from '@atlaskit/button';

export interface Props {
  clientId: string;
  clientSecret: string;
  accessToken: string;
  refreshToken: string;
  onAccessTokenChange: (token: string) => any;
}

export interface State {
  accessTokenFetching?: boolean;
  accessToken: string;
}

class Credentials extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      accessToken: props.accessToken
    };
  }

  refreshToken = () => {
    this.setState({accessTokenFetching: true}, async () => {
      const {
        clientId,
        clientSecret,
        refreshToken
      } = this.props;

      try {
        const res = await fetch(`https://auth.atlassian.com/oauth/token`, {
          method: 'POST',
          headers: {
            'content-type': 'application/json'
          },
          body: JSON.stringify({
            grant_type: 'refresh_token',
            client_id: clientId,
            client_secret: clientSecret,
            refresh_token: refreshToken
          })
        });

        const json = await res.json();

        this.setState({
          accessTokenFetching: false,
          accessToken: json.access_token
        });
      } catch (err) {
        this.setState({accessTokenFetching: false});
      }
    });
  }

  onTokenUpdate = (evt) => {
    this.setState({accessToken: evt.target.value});
  }

  undoTokenChange = () => {
    this.setState({accessToken: this.props.accessToken});
  }

  componentDidUpdate(prevProps: Props, prevState: State): void {
    const {accessToken} = this.state;

    if (accessToken !== prevState.accessToken) {
      this.props.onAccessTokenChange(accessToken);
    }
  }

  render(): JSX.Element | null {
    const {
      accessToken,
      accessTokenFetching
    } = this.state;

    return (
      <div className="appkey">
        <h1>App credentials</h1>

        <FieldTextStateless
          shouldFitContainer
          onChange={this.onTokenUpdate}
          label="Access Token"
          value={accessTokenFetching ? 'Please wait...' : accessToken}
          isReadOnly={accessTokenFetching}
        />
        <Button
          isDisabled={accessTokenFetching}
          className="appkey__button"
          onClick={this.refreshToken}>Refresh token</Button>
        <Button
          isDisabled={accessToken === this.props.accessToken}
          className="appkey__button"
          onClick={this.undoTokenChange}>Undo token changes</Button>
      </div>
    );
  }
}

export default Credentials;
