import SingleSelect from '@atlaskit/single-select';
import * as React from 'react';
import { DataProvider } from '../util';

export interface Site {
  cloudId: string;
  name: string;
}

export interface SiteProps {
  accessToken: string;
  onSiteSelect: (cloudId: string, siteName: string) => any;
  cloudId: string | null;
}

export type Product = 'jira' | 'confluence';

export interface Props extends SiteProps {
  product: Product;
}

export interface State {
  fetching?: boolean;
  sites: Array<Site>;
}

export const scopesByProduct: { [K in Props['product']]: Array<string> } = {
  'jira': [
    'read:jira-user',
    'read:jira-work',
    'write:jira-work',
    'manage:jira-project',
    'manage:jira-configuration'
  ],
  'confluence': [
    'read:confluence-content.all',
    'read:confluence-content.summary',
    'write:confluence-content',
    'read:confluence-space.summary',
    'write:confluence-space',
    'write:confluence-file',
    'read:confluence-props',
    'write:confluence-props',
    'search:confluence',
    'manage:confluence-configuration'
  ]
};

const hasScopesForProduct = (product: Product) =>
  (site) => !!site.scopes.filter((scope) => scopesByProduct[product].indexOf(scope) >= 0).length;

class ProductSite extends React.Component<Props, State> {
  private dataProvider: DataProvider;

  constructor(props: Props) {
    super(props);

    this.dataProvider = new DataProvider(props.accessToken);
    this.state = {
      sites: []
    };
  }

  onSiteSelect = (data) => {
    const cloudId = data.item.value;
    const siteName = data.item.content;
    this.props.onSiteSelect(cloudId, siteName);
  }

  async getSites(): Promise<void> {
    this.setState({fetching: true});

    try {
      const json = await this.dataProvider.fetch('/introspection/sites');
      const sites: Array<Site> = json
        .filter(hasScopesForProduct(this.props.product))
        .map((site) => {
        return {
          cloudId: site.id,
          name: site.name
        };
      });

      this.setState({sites});
    } finally {
      this.setState({fetching: false});
    }
  }

  componentWillReceiveProps(nextProps: Props): void {
    if (this.props.accessToken !== nextProps.accessToken) {
      this.dataProvider.setAccessToken(nextProps.accessToken);
    }
  }

  async componentDidMount(): Promise<void> {
    await this.getSites();
  }

  render(): JSX.Element | null {
    const {cloudId} = this.props;
    const {fetching, sites} = this.state;

    if (fetching) {
      return (
        <div className="playground__field">
          <span>Please wait...</span>
        </div>
      );
    }

    const items = [{
      items: sites.map((site) => ({
        content: site.name,
        value: site.cloudId
      }))
    }];

    return (
      <div className="playground__field">
        <SingleSelect
          hasAutocomplete
          items={items}
          label="Site"
          onSelected={this.onSiteSelect}
        />
      </div>
    );
  }
}

export const JiraSite = (props: SiteProps) => {
  const productProps = { ...props, product: 'jira' };
  return (<ProductSite {...productProps as Props } />);
};

export const ConfluenceSite = (props: SiteProps) => {
  const productProps = { ...props, product: 'confluence' };
  return (<ProductSite {...productProps as Props } />);
};
