import SingleSelect from '@atlaskit/single-select';
import * as React from 'react';
import { Component } from 'react';
import { DataProvider } from '../util';

interface Props {
  cloudId: string | null;
  spaceKey: string | null;
  accessToken: string;
  onPageSelect: (pageId: string) => any;
}

interface Page {
  id: string;
  type: string;
  status: string;
  title: string;
}

interface State {
  fetching?: boolean;
  pages: Array<Page>;
}

class ConfluencePage extends Component<Props, State> {
  private dataProvider: DataProvider;

  constructor(props: Props) {
    super(props);

    this.dataProvider = new DataProvider(props.accessToken);

    this.state = {
      fetching: false,
      pages: []
    };
  }

  onPageSelect = (data) => {
    const pageId = data.item.value;
    this.props.onPageSelect(pageId);
  }

  async getPages(): Promise<void> {
    if (!this.props.cloudId || !this.props.spaceKey) {
      return;
    }

    this.setState({ fetching: true });

    const { cloudId, spaceKey } = this.props;

    try {
      const json = await this.dataProvider.fetch(`/confluence/${cloudId}/space/${spaceKey}/content`);
      const pages: Array<Page> = json.page.results
        .map((page) => {
          return {
            id: page.id,
            type: page.type,
            status: page.status,
            title: page.title
          };
        });

      this.setState({ pages });
    } finally {
      this.setState({ fetching: false });
    }
  }

  componentWillReceiveProps(nextProps: Props): void {
    if (this.props.accessToken !== nextProps.accessToken) {
      this.dataProvider.setAccessToken(nextProps.accessToken);
    }
  }

  async componentDidMount(): Promise<void> {
    await this.getPages();
  }

  async componentDidUpdate(prevProps: Props): Promise<void> {
    if (this.props.spaceKey !== prevProps.spaceKey) {
      await this.getPages();
    }
  }

  render(): JSX.Element | null {
    const { fetching, pages } = this.state;

    if (fetching) {
      return (
        <div className="playground__field">
          <span>Please wait...</span>
        </div>
      );
    }

    const items = [{
      items: pages.map((page) => ({
        content: page.title,
        value: page.id
      }))
    }];

    return (
      <div className="playground__field">
        <SingleSelect
          hasAutocomplete
          items={items}
          label="Page"
          onSelected={this.onPageSelect}
        />
      </div>
    );
  }
}

export default ConfluencePage;
