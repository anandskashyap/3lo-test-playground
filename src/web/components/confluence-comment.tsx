import Button from '@atlaskit/button';
import { FieldTextAreaStateless } from '@atlaskit/field-text-area';
import * as React from 'react';

import { DataProvider } from '../util';

export interface Props {
  accessToken: string;
  site: string | null;
  space: string | null;
  page: string | null;
}

export interface State {
  saving?: boolean;
  comment?: string;
  link?: string;
}

class ConfluenceComment extends React.Component<Props, State> {
  private dataProvider: DataProvider;
  state: State = {};

  constructor(props: Props) {
    super(props);
    this.dataProvider = new DataProvider(props.accessToken);
  }

  onComment = (evt) => {
    this.setState({ comment: evt.target.value });
  }

  onCommentSend = async () => {
    const { site, space, page } = this.props;
    this.setState({ saving: true });

    try {
      const { _links } = await this.dataProvider.fetch(`/confluence/${site}/space/${space}/content/${page}/comment`, {
        method: 'POST',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify({
          comment: this.state.comment
        })
      });

      this.setState({ link: _links.self });
    } finally {
      this.setState({ saving: false });
    }
  }

  render(): JSX.Element | null {
    const { space, site, page } = this.props;
    const { saving, comment, link } = this.state;

    if (!site || !space || !page) {
      return null;
    }

    return (
      <div className="playground__field">
        <FieldTextAreaStateless
          label="Comment"
          onChange={this.onComment}
          enableResize
          shouldFitContainer
          minimumRows={5}
          value={comment || ''}
        />
        <Button
          isDisabled={saving}
          className="appkey__button"
          onClick={this.onCommentSend}>Add</Button>
        {link &&
          <div className="playground__field">
            Comments are being added to <a target="_blank" href={link}>this page</a>
          </div>
        }
      </div>
    );
  }
}

export default ConfluenceComment;
