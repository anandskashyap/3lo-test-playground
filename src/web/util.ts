export class DataProvider {
  private accessToken: string;

  constructor(accessToken = '') {
    this.setAccessToken(accessToken);
  }

  setAccessToken(accessToken: string): void {
    this.accessToken = accessToken;
  }

  async fetch(url: string, init?: RequestInit): Promise<any> {
    const urlObj = new URL(url, location.origin);

    if (!urlObj.searchParams.get('token')) {
      urlObj.searchParams.set('token', this.accessToken);
    }

    try {
      const res = await fetch(urlObj.toString(), init);

      if (!res.ok) {
        const responseText = await res.text();
        throw new Error(`Response failed (${res.status}): ${responseText}`);
      }
      return res.status === 204
        ? null
        : await res.json();
    } catch (err) {
      alert(err.message);
      throw err;
    }
  }
}
