import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Button from '@atlaskit/button';
import FieldText from '@atlaskit/field-text';
import MultiSelect from '@atlaskit/multi-select';
import '@atlaskit/css-reset';

import Wrapper from './components/wrapper';

type TestScreenState = {
  selectedScopes: Array<string>;
  clientId: string;
  clientSecret: string;
  isRedirecting?: boolean;
};

declare global {
  interface Window {
    environmentQualifier: string;
  }
}

const scopeItems = [
  {
    items: [
      'read:confluence-content.all',
      'read:confluence-content.summary',
      'write:confluence-content',
      'read:confluence-space.summary',
      'write:confluence-space',
      'write:confluence-file',
      'read:confluence-props',
      'write:confluence-props',
      'search:confluence',
      'manage:confluence-configuration',
      'participate:conversation',
      'manage:conversation',
      'read:jira-user',
      'read:jira-work',
      'write:jira-work',
      'manage:jira-project',
      'manage:jira-configuration',
      'read:servicedesk-request',
      'write:servicedesk-request',
      'manage:servicedesk-customer'
    ].map((scope) => {
      return {
        content: scope,
        value: scope
      };
    })
  }
];


class TestScreen extends React.Component<{}, TestScreenState> {
  state: TestScreenState = {
    selectedScopes: [],
    clientId: sessionStorage.getItem('clientId') || '',
    clientSecret: sessionStorage.getItem('clientSecret') || ''
  };

  onFieldTextChange = (stateKey) => (evt) => {
    sessionStorage.setItem(stateKey, evt.target.value);

    this.setState({
      [stateKey]: evt.target.value
    });
  }

  selectionChange = (action) => {
    this.setState({
      selectedScopes: action.items.map((item) => item.value)
    });
  }


  performRedirect = () => {
    this.setState({ isRedirecting: true }, () => {
      const { clientId, clientSecret, selectedScopes } = this.state;
      const scope = selectedScopes.length
        ? selectedScopes.concat(['offline_access'])
        : [];

      const scopeSerialized = scope.join('%20');

      // do not pass client id and secret in state
      // this is only for testing purposes
      const state = {
        custom: 'some data',
        id: clientId,
        secret: clientSecret,
        playground: scopeSerialized.includes('jira') ? 'jira' : 'confluence'
      };

      window.location.href = `https://auth${window.environmentQualifier}.atlassian.com/authorize?`
        + `audience=api${window.environmentQualifier}.atlassian.com`
        + '&client_id=' + clientId
        + `&scope=${scopeSerialized}`
        + '&redirect_uri=' + encodeURI(`${window.location.href}callback`)
        + '&state=' + encodeURIComponent(JSON.stringify(state))
        + '&response_type=code'
        + '&prompt=consent';
    });
  }

  render(): JSX.Element {
    const { clientId, clientSecret, isRedirecting } = this.state;
    const isButtonDisabled = !clientId || !clientSecret || isRedirecting;

    return (
      <Wrapper>
        <div className="appkey">
          <h1>Start flow</h1>

          <FieldText
            required
            shouldFitContainer
            onChange={this.onFieldTextChange('clientId')}
            label="Client ID"
            value={clientId}
            invalidMessage='Copy it from "Enabled APIs" tab of your app management page'
          />
          <FieldText
            required
            shouldFitContainer
            type="password"
            onChange={this.onFieldTextChange('clientSecret')}
            label="Client secret"
            value={clientSecret}
            invalidMessage='Copy it from "Enabled APIs" tab of your app management page'
          />
          <MultiSelect
            shouldFitContainer
            items={scopeItems}
            label="Choose scopes"
            onSelectedChange={this.selectionChange}
          />
          <Button
            isDisabled={isButtonDisabled}
            className="appkey__start"
            appearance="primary"
            onClick={this.performRedirect}>{isRedirecting ? 'Please wait...' : 'Start'}</Button>
        </div>
      </Wrapper>
    );
  }
}

ReactDOM.render(<TestScreen/>, document.getElementById('app-root'));
